"use strict"
// 1. Логічний оператор це символ або ключове слово, яке використовується для встановлення логічних зв'язків між виразами чи умовами в програмуванні та логіці.
// 2. В Javascript існує чотири логічні оператори: || (або), && (і), ! (не), ?? (оператор null-об'єднання).

const userAge = prompt("Введіть свій вік");
console.log(userAge);

if (!userAge || isNaN(userAge)) {
    alert("Ви ввели не число");
} else if (userAge < 3 || userAge > 100) {
    alert("ваш вік не відповідає дійсності введіть коректний вік");
} else if (userAge < 12) {
    alert("Ви є дитиною");
} else if (userAge < 18) {
    alert("Ви є підлітком");
} else {
    alert("Ви є дорослим");
}

const month = prompt("Введіть місяць");

switch (month) {
    case "січень": {
        alert("31")
        break;
    }

    case "лютий": {
        alert("28")
        break;
    }

    case "березень": {
        alert("31")
        break;
    }

    case "квітень": {
        alert("30")
        break;
    }

    case "травень": {
        alert("31")
        break;
    }

    case "червень": {
        alert("30")
        break;
    }

    case "липень": {
        alert("31")
        break;
    }

    case "серпень": {
        alert("31")
        break;
    }

    case "вересень": {
        alert("30")
        break;
    }

    case "жовтень": {
        alert("31")
        break;
    }

    case "листопад": {
        alert("30")
        break;
    }

    case "грудень": {
        alert("31")
        break;
    }

    default:
        alert("Ви ввели невірне значення");

}



